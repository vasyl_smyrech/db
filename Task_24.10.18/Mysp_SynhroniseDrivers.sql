USE [Motor Transport Enterprise];
GO
ALTER PROC mysp_SynhroniseDrivers 
@Request XML
AS 
DECLARE @docnum INT; 
EXEC sp_XML_preparedocument @docnum output, @Request;

BEGIN TRY  
	BEGIN TRANSACTION 
	MERGE dbo.DriversCopy trg 
	USING
		(SELECT * FROM OPENXML(@docnum, '/Drivers/Driver', 2) 
			WITH ([IdDriver] [int],
				FirstName [nvarchar](15),
				MiddleName [nvarchar](20),
				SecondName [nvarchar](20),
				DateOfBirth [date])) AS src
	ON trg.[IdDriver]=src.IdDriver

	WHEN MATCHED THEN
	  UPDATE SET
		trg.FirstName = src.FirstName,
		trg.MiddleName = src.MiddleName,
		trg.SecondName = src.SecondName,
		trg.DateOfBirth = src.DateOfBirth
	WHEN NOT MATCHED THEN 
	  INSERT(IdDriver,FirstName,MiddleName,SecondName,DateOfBirth)
	  VALUES(src.IdDriver,src.FirstName,src.MiddleName,src.SecondName, src.DateOfBirth);

	EXECUTE sp_XML_removedocument @docnum;
	 
	COMMIT TRANSACTION 
END TRY  
BEGIN CATCH  
	SELECT 
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;
	EXECUTE sp_XML_removedocument @docnum;
	ROLLBACK TRANSACTION 
END CATCH;
GO
DECLARE @Request XML;
SET @Request = (SELECT * FROM Drivers
FOR XML PATH ('Driver'), ROOT ('Drivers'));
EXEC mysp_SynhroniseDrivers @Request;