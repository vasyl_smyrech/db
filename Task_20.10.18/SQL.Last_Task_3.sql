USE [Motor Transport Enterprise];
GO
CREATE TYPE ReservDriverType AS TABLE
(
    [First Name] [nvarchar](32) NOT NULL,
	[Middle Name] [nvarchar](20),
	[Second Name] [nvarchar](20) NOT NULL,
	[Date of Birth] [date],
	[TableName] [nvarchar](50) NOT NULL
)
GO
CREATE PROC ReserveDriverAddRow 
 @ReserveDriversType ReservDriverType READONLY
AS
BEGIN
 BEGIN  
 DECLARE @FirstName nvarchar(32),
 @MiddleName nvarchar(20),
 @SecondName nvarchar(20),
 @DateOfBirth date,
 @TableName nvarchar(50),
 @DriverID nvarchar(32)
 DECLARE ResDriverCursor CURSOR LOCAL
 FOR SELECT [First Name], [Middle Name], [Second Name], [Date of Birth], [TableName] FROM @ReserveDriversType
 OPEN ResDriverCursor
   FETCH NEXT FROM ResDriverCursor
 INTO @FirstName, @MiddleName, @SecondName, @DateOfBirth, @TableName
   WHILE @@FETCH_STATUS = 0
   BEGIN
     EXEC GenerID @TableName, @DriverID OUTPUT;
     INSERT INTO ReserveDrivers ([First Name], [Middle Name], [Second Name], [Date of Birth], [DriverID])     
	 VALUES (@FirstName, @MiddleName, @SecondName, @DateOfBirth, @DriverID);
     FETCH NEXT FROM ResDriverCursor
     INTO @FirstName, @MiddleName, @SecondName, @DateOfBirth, @TableName;
   END
   CLOSE ResDriverCursor;
   DEALLOCATE ResDriverCursor;
 END
END

 GO


 DECLARE @TableDrivers  AS ReservDriverType;

 INSERT INTO @TableDrivers  VALUES ('Bob', NULL, 'Marley', NULL, 'REserveDrivers');
 INSERT INTO @TableDrivers  VALUES ('Katia', 'Ivanivna', 'Marley', NULL, 'REserveDrivers');
 INSERT INTO @TableDrivers  VALUES ('Bob', NULL, 'Marley', NULL, 'REserveDrivers');
 
 EXEC ReserveDriverAddRow @TableDrivers;

 SELECT * FROM ReserveDrivers;