USE Northwind;

INSERT INTO Suppliers (CompanyName, City, Region, Country, Phone)
VALUES ('Increacer Company', 'Boston', 'MA', 'USA', '100255552237'),
 ('Beaver Barn', 'Boston', 'MA', 'USA', '100255578255'),
 ('Deap & Shalow', 'New Orleans', 'LA', 'USA', '10026845223'),
 ('Food Of Seas', 'Ann Arbor', 'MI', 'USA', '10025525788'),
 ('InterTool', 'Ann Arbor', 'MI', 'USA', '10025551423');

SELECT Region, City, COUNT(*) AS
ManufacturerCount
FROM Suppliers
WHERE Country ='USA'
GROUP BY Region, City
HAVING COUNT(Region) > 1;