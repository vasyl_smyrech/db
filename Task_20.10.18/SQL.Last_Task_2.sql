USE [Motor Transport Enterprise];
GO
CREATE PROCEDURE GenerID
 @ReserveDriver nvarchar(32),
 @DriverID nvarchar(32) OUTPUT
AS
BEGIN
DECLARE @myid uniqueidentifier = NEWID();
SELECT  @DriverID = LEFT(@ReserveDriver, 6) + Convert(NVARCHAR (30), GETDATE(), 112) + LEFT(CONVERT(nvarchar(max), @myid), 16);
END