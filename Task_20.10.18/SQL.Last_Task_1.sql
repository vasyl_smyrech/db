USE [Motor Transport Enterprise];
/* Task_20.10.18.ITEM_1 is done by rewriting the table. */
ALTER TABLE ReserveDrivers -- adds the column with datatype nvarchar for containing generated PRIMARY KEYs
ADD ID nvarchar(30);
GO
UPDATE ReserveDrivers
SET ID = CAST(ReserveDrivers.DriverID as nvarchar(30)); --converts and copies data from DriverID to ID
SELECT * INTO ReserveDrivers2 FROM ReserveDrivers; -- copies the table
ALTER TABLE ReserveDrivers2 DROP COLUMN DriverID; -- deletes column 'Id Driver' from the table
DROP TABLE ReserveDrivers; -- deletes original table
EXEC sp_rename 'ReserveDrivers2', 'ReserveDrivers';  -- substitutes the coppy table with needed column instead original table
ALTER TABLE ReserveDrivers
ALTER COLUMN ID nvarchar(30) NOT NULL -- change column to unnullable
EXEC sp_rename 'ReserveDrivers.ID' , 'DriverID';
