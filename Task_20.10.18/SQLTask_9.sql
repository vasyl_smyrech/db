USE Northwind;

SELECT em.LastName, em.FirstName, te.TerritoryDescription, re.RegionDescription
FROM Employees AS em 
    LEFT JOIN EmployeeTerritories AS et ON em.EmployeeID = et.EmployeeID
	LEFT JOIN Territories AS te ON et.TerritoryID = te.TerritoryID
	LEFT JOIN Region AS re ON te.RegionID = re.RegionID 