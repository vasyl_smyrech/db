USE Northwind;

SELECT DISTINCT re.RegionID, re.RegionDescription
FROM Region AS re 
    LEFT JOIN Territories AS te ON re.RegionID = te.RegionID
    LEFT JOIN EmployeeTerritories AS et ON et.TerritoryID = te.TerritoryID
	LEFT JOIN Employees AS em ON em.EmployeeID = et.EmployeeID
    WHERE em.EmployeeID IS NULL