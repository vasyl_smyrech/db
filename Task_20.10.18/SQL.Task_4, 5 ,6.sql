USE Northwind;

(SELECT DISTINCT * FROM Customers cu WHERE cu.Region = 'SP') --4
UNION
(SELECT DISTINCT * FROM Customers cu WHERE cu.City = 'Madrid')

SELECT EmployeeID, LastName, FirstName FROM Employees -- 5
WHERE Region = 'WA'
EXCEPT
SELECT EmployeeID, LastName, FirstName FROM Employees
WHERE City = 'Seattle'

SELECT * FROM Products -- 6
WHERE UnitPrice > (SELECT AVG (UnitPrice) FROM Products); 
