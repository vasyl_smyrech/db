USE Northwind;

SELECT CategoryName,
(SELECT MAX(UnitPrice) FROM Products AS pr 
WHERE ca.CategoryID = pr.CategoryID) AS UnitPrice 
FROM Categories AS ca;
