USE [Motor Transport Enterprise];

INSERT INTO VehiclesNames -- 1.a
VALUES ('KRAZ', 'Lisnyk'),('MAN','N25T'),('GAZ','53'),('IKARUS','T22'),('LAZ','Turist');

INSERT INTO Drivers ([First Name],[Second Name]) --1.b
VALUES ('Petrus','Krutich');

-- It would made item 'c' in the task faster
-- SELECT *
-- INTO ReserveDrivers
-- FROM Drivers
CREATE TABLE ReserveDrivers -- 1.c
(  
[Id Driver]	    int	 NOT NULL IDENTITY(0,1),
[First Name]	nvarchar(15) NOT NULL,
[Middle Name]	nvarchar(20),
[Second Name]	nvarchar(20) NOT NULL,
[Date of Birth]	date
PRIMARY KEY ([Id Driver])
);
INSERT INTO ReserveDrivers ([First Name], [Middle Name], [Second Name], [Date of Birth])
SELECT [First Name], [Middle Name], [Second Name], [Date of Birth] FROM Drivers;
