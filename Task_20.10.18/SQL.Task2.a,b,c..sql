USE [Motor Transport Enterprise];
UPDATE VehiclesNames -- Task 2-a
SET Model = 'D2555' 
WHERE Manufacturer = 'RAF';

UPDATE  AuthorisedMass -- Task 2-b
SET [Load-in Ability (tons)] = [Load-in Ability (tons)] * 0.8
WHERE [Load-in Ability (tons)] > 40;

UPDATE AuthorisedMass
SET [Load-in Ability (tons)] = [Load-in Ability (tons)] * 0.8 -- Task 2-c
FROM AuthorisedMass AS am LEFT JOIN
VehiclesNames AS vn ON am.[Id Mass] = vn.[Id VehicleName]
WHERE  vn.Model = 'Kopra 55';
