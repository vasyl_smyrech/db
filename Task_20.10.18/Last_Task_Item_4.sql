USE [Motor Transport Enterprise];
DROP TRIGGER dbo.reserveDrivers_Insert;
GO
ALTER TRIGGER reserveDrivers_Insert 
ON dbo.ReserveDrivers INSTEAD OF INSERT
AS 
BEGIN
 DECLARE @TableName nvarchar(50),
         @DriverID nvarchar(32)
 SELECT @TableName = DriverID FROM INSERTED
 EXEC GenerID @TableName, @DriverID OUTPUT;
INSERT INTO ReserveDrivers ([First Name], [Middle Name], [Second Name], [Date of Birth], [DriverID])  
SELECT [First Name], [Middle Name], [Second Name], [Date of Birth], @DriverID FROM inserted;
END
GO
EXEC sp_settriggerorder @order='first', @triggername= 'reserveDrivers_Insert' , @stmttype='insert', @namespace = 'DATABASE'; 
GO
INSERT INTO dbo.ReserveDrivers ([First Name], [Middle Name], [Second Name], [Date of Birth], [DriverID]) 
VALUES ('Misha', 'Fedotovich', 'Krasyi', '1978-02-25', 'ReserveDrivers');
-- I don't understand what means error massage beneath. Trigger inserts rows in the table correctly.