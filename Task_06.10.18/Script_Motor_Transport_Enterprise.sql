USE [master]
GO
/****** Object:  Database [Motor Transport Enterprise]    Script Date: 09.10.2018 17:51:34 ******/
CREATE DATABASE [Motor Transport Enterprise]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Motor Transport Enterprise', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\Motor Transport Enterprise.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Motor Transport Enterprise_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\Motor Transport Enterprise_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Motor Transport Enterprise] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Motor Transport Enterprise].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Motor Transport Enterprise] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Motor Transport Enterprise] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Motor Transport Enterprise] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Motor Transport Enterprise] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Motor Transport Enterprise] SET ARITHABORT OFF 
GO
ALTER DATABASE [Motor Transport Enterprise] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Motor Transport Enterprise] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Motor Transport Enterprise] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Motor Transport Enterprise] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Motor Transport Enterprise] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Motor Transport Enterprise] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Motor Transport Enterprise] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Motor Transport Enterprise] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Motor Transport Enterprise] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Motor Transport Enterprise] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Motor Transport Enterprise] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Motor Transport Enterprise] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Motor Transport Enterprise] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Motor Transport Enterprise] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Motor Transport Enterprise] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Motor Transport Enterprise] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Motor Transport Enterprise] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Motor Transport Enterprise] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Motor Transport Enterprise] SET  MULTI_USER 
GO
ALTER DATABASE [Motor Transport Enterprise] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Motor Transport Enterprise] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Motor Transport Enterprise] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Motor Transport Enterprise] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Motor Transport Enterprise] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Motor Transport Enterprise] SET QUERY_STORE = OFF
GO
USE [Motor Transport Enterprise]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [Motor Transport Enterprise]
GO
/****** Object:  Table [dbo].[AuthorisedMass]    Script Date: 09.10.2018 17:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuthorisedMass](
	[Id Mass] [int] IDENTITY(0,1) NOT NULL,
	[Load-in Ability (tons)] [decimal](3, 1) NOT NULL,
 CONSTRAINT [PK_AuthorisedMass] PRIMARY KEY CLUSTERED 
(
	[Id Mass] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DriveCategory]    Script Date: 09.10.2018 17:51:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DriveCategory](
	[Class (Category)] [char](3) NOT NULL,
	[Category Description] [nvarchar](100) NULL,
 CONSTRAINT [PK_DriveCategory] PRIMARY KEY CLUSTERED 
(
	[Class (Category)] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Drivers]    Script Date: 09.10.2018 17:51:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Drivers](
	[Id Driver] [int] IDENTITY(0,1) NOT NULL,
	[Fifst Name] [nvarchar](15) NOT NULL,
	[Middle Name] [nvarchar](20) NULL,
	[Second Name] [nvarchar](20) NOT NULL,
	[Date of Birth] [date] NULL,
 CONSTRAINT [PK_Drivers_1] PRIMARY KEY CLUSTERED 
(
	[Id Driver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_Drivers] UNIQUE NONCLUSTERED 
(
	[Id Driver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DriversAndCategories]    Script Date: 09.10.2018 17:51:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DriversAndCategories](
	[Id Driver] [int] NOT NULL,
	[Class (Category)] [char](3) NOT NULL,
 CONSTRAINT [PK_DriversAndCategories] PRIMARY KEY CLUSTERED 
(
	[Id Driver] ASC,
	[Class (Category)] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SeatsNumber]    Script Date: 09.10.2018 17:51:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SeatsNumber](
	[Seat Number (Including Driver)] [smallint] NOT NULL,
 CONSTRAINT [PK_SeatsNumber] PRIMARY KEY CLUSTERED 
(
	[Seat Number (Including Driver)] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VehicleNamesAndAuthorisedMass]    Script Date: 09.10.2018 17:51:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VehicleNamesAndAuthorisedMass](
	[Id VehicleName] [int] NOT NULL,
	[Id Mass] [int] NOT NULL,
 CONSTRAINT [PK_VehicleNamesAndAuthorisedMass] PRIMARY KEY CLUSTERED 
(
	[Id VehicleName] ASC,
	[Id Mass] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VehicleNamesAndCategory]    Script Date: 09.10.2018 17:51:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VehicleNamesAndCategory](
	[Id VehicleName] [int] NOT NULL,
	[Class (Category)] [char](3) NOT NULL,
 CONSTRAINT [PK_VehicleNamesAndCategory] PRIMARY KEY CLUSTERED 
(
	[Id VehicleName] ASC,
	[Class (Category)] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VehiclesNameAndVehicleType]    Script Date: 09.10.2018 17:51:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VehiclesNameAndVehicleType](
	[Id VehicleName] [int] IDENTITY(0,1) NOT NULL,
	[Vehicle Type] [nvarchar](15) NOT NULL,
 CONSTRAINT [PK_VehiclesNameAndVehicleType] PRIMARY KEY CLUSTERED 
(
	[Id VehicleName] ASC,
	[Vehicle Type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VehiclesNames]    Script Date: 09.10.2018 17:51:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VehiclesNames](
	[Id VehicleName] [int] IDENTITY(0,1) NOT NULL,
	[Manufacturer] [nvarchar](50) NOT NULL,
	[Model] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_VehiclesNames] PRIMARY KEY CLUSTERED 
(
	[Id VehicleName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VehiclesNamesAndSeatsNumber]    Script Date: 09.10.2018 17:51:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VehiclesNamesAndSeatsNumber](
	[Id VehicleName] [int] NOT NULL,
	[Seat Number] [smallint] NOT NULL,
 CONSTRAINT [PK_VehiclesNamesAndSeatsNumber] PRIMARY KEY CLUSTERED 
(
	[Id VehicleName] ASC,
	[Seat Number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VehicleType]    Script Date: 09.10.2018 17:51:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VehicleType](
	[Vehicle Type] [nvarchar](15) NOT NULL,
 CONSTRAINT [PK_VehicleType] PRIMARY KEY CLUSTERED 
(
	[Vehicle Type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_DriveCategory]    Script Date: 09.10.2018 17:51:37 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_DriveCategory] ON [dbo].[DriveCategory]
(
	[Class (Category)] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DriversAndCategories]    Script Date: 09.10.2018 17:51:37 ******/
CREATE NONCLUSTERED INDEX [IX_DriversAndCategories] ON [dbo].[DriversAndCategories]
(
	[Id Driver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_SeatsNumber]    Script Date: 09.10.2018 17:51:37 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_SeatsNumber] ON [dbo].[SeatsNumber]
(
	[Seat Number (Including Driver)] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DriversAndCategories]  WITH CHECK ADD  CONSTRAINT [FK_DriversAndCategories_DriveCategory] FOREIGN KEY([Class (Category)])
REFERENCES [dbo].[DriveCategory] ([Class (Category)])
GO
ALTER TABLE [dbo].[DriversAndCategories] CHECK CONSTRAINT [FK_DriversAndCategories_DriveCategory]
GO
ALTER TABLE [dbo].[DriversAndCategories]  WITH CHECK ADD  CONSTRAINT [FK_DriversAndCategories_Drivers] FOREIGN KEY([Id Driver])
REFERENCES [dbo].[Drivers] ([Id Driver])
GO
ALTER TABLE [dbo].[DriversAndCategories] CHECK CONSTRAINT [FK_DriversAndCategories_Drivers]
GO
ALTER TABLE [dbo].[VehicleNamesAndAuthorisedMass]  WITH CHECK ADD  CONSTRAINT [FK_VehicleNamesAndAuthorisedMass_AuthorisedMass] FOREIGN KEY([Id Mass])
REFERENCES [dbo].[AuthorisedMass] ([Id Mass])
GO
ALTER TABLE [dbo].[VehicleNamesAndAuthorisedMass] CHECK CONSTRAINT [FK_VehicleNamesAndAuthorisedMass_AuthorisedMass]
GO
ALTER TABLE [dbo].[VehicleNamesAndAuthorisedMass]  WITH CHECK ADD  CONSTRAINT [FK_VehicleNamesAndAuthorisedMass_VehiclesNames] FOREIGN KEY([Id VehicleName])
REFERENCES [dbo].[VehiclesNames] ([Id VehicleName])
GO
ALTER TABLE [dbo].[VehicleNamesAndAuthorisedMass] CHECK CONSTRAINT [FK_VehicleNamesAndAuthorisedMass_VehiclesNames]
GO
ALTER TABLE [dbo].[VehicleNamesAndCategory]  WITH CHECK ADD  CONSTRAINT [FK_CategoryVehicles] FOREIGN KEY([Id VehicleName])
REFERENCES [dbo].[VehiclesNames] ([Id VehicleName])
GO
ALTER TABLE [dbo].[VehicleNamesAndCategory] CHECK CONSTRAINT [FK_CategoryVehicles]
GO
ALTER TABLE [dbo].[VehicleNamesAndCategory]  WITH CHECK ADD  CONSTRAINT [FK_VehicleCategory] FOREIGN KEY([Class (Category)])
REFERENCES [dbo].[DriveCategory] ([Class (Category)])
GO
ALTER TABLE [dbo].[VehicleNamesAndCategory] CHECK CONSTRAINT [FK_VehicleCategory]
GO
ALTER TABLE [dbo].[VehiclesNameAndVehicleType]  WITH CHECK ADD  CONSTRAINT [FK_VehiclesNameAndVehicleType_VehiclesNames] FOREIGN KEY([Id VehicleName])
REFERENCES [dbo].[VehiclesNames] ([Id VehicleName])
GO
ALTER TABLE [dbo].[VehiclesNameAndVehicleType] CHECK CONSTRAINT [FK_VehiclesNameAndVehicleType_VehiclesNames]
GO
ALTER TABLE [dbo].[VehiclesNameAndVehicleType]  WITH CHECK ADD  CONSTRAINT [FK_VehiclesNameAndVehicleType_VehicleType] FOREIGN KEY([Vehicle Type])
REFERENCES [dbo].[VehicleType] ([Vehicle Type])
GO
ALTER TABLE [dbo].[VehiclesNameAndVehicleType] CHECK CONSTRAINT [FK_VehiclesNameAndVehicleType_VehicleType]
GO
ALTER TABLE [dbo].[VehiclesNamesAndSeatsNumber]  WITH CHECK ADD  CONSTRAINT [FK_VehiclesNamesAndSeatsNumber_SeatsNumber] FOREIGN KEY([Seat Number])
REFERENCES [dbo].[SeatsNumber] ([Seat Number (Including Driver)])
GO
ALTER TABLE [dbo].[VehiclesNamesAndSeatsNumber] CHECK CONSTRAINT [FK_VehiclesNamesAndSeatsNumber_SeatsNumber]
GO
ALTER TABLE [dbo].[VehiclesNamesAndSeatsNumber]  WITH CHECK ADD  CONSTRAINT [FK_VehiclesNamesAndSeatsNumber_VehiclesNames] FOREIGN KEY([Id VehicleName])
REFERENCES [dbo].[VehiclesNames] ([Id VehicleName])
GO
ALTER TABLE [dbo].[VehiclesNamesAndSeatsNumber] CHECK CONSTRAINT [FK_VehiclesNamesAndSeatsNumber_VehiclesNames]
GO
USE [master]
GO
ALTER DATABASE [Motor Transport Enterprise] SET  READ_WRITE 
GO
